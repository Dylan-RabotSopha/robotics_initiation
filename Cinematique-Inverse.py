"""BERGER Jordan            Représentant:Rémi FABRE
    Ynov campus
    But: TP robotique, travail sur la cinématique inverse
"""

import math

l1 = 51
l2 = 63.7
l3 = 93
theta1 = float(input("Saisissez Theta1 : "))
theta_2 = float(input("Saisissez Theta2 : "))
theta_3 = float(input("Saisissez Theta3 : "))
 
#Conversion des degrés en radians /!\
theta1 = theta1*math.pi/180
theta_2 = theta_2*math.pi/180
theta_3 = theta_3*math.pi/180

theta2c= -20.69*math.pi/180
theta3c= (90*math.pi/180)+theta2c-(5.06*math.pi/180)

theta2 = theta_2-theta2c
theta3 = -(theta_3-theta3c)

x3= math.cos(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
y3= math.sin(theta1)*(l1+l2*math.cos(theta2)+l3*math.cos(theta2+theta3))
z3= l2*math.sin(theta2)+l3*math.sin(theta2+theta3)
"""
print("Valeur de x3 : ", x3)
print("Valeur de y3 : ", y3)
print("Valeur de z3 : ", z3)"""

print("P3 : " ,x3,";", y3,";", z3)


######Cinematique inverse##########

#recherche de l'angle theta1
d13=math.sqrt(x3*x3+y3*y3)-l1
teta1=(math.asin(y3/(d13+l1)))*(180/math.pi)

#Recherche de l'angle theta2, avec correction
d=math.sqrt(d13*d13+z3*z3)

a=(math.asin(z3/d))*180/math.pi
c=(math.acos((d*d+l2*l2-l3*l3)/(2*d*l2)))*180/math.pi

theta2c=-20.69
teta2=a-c+theta2c

#Recherche de l'angle theta3,avec correction
teta4=math.acos((l2*l2+l3*l3-d*d)/(2*l2*l3))*(180/math.pi)

teta3=180-teta4-(theta3c*180/math.pi)

#Affichage des valeurs
print("Valeur de teta1=",teta1)
print("Valeur de teta2=", teta2)
print("Valeur de teta3=", teta3)